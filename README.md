# Fraud detection of government procurement in Russia
## term project 2018 - 2019
---
run virtualenv
```shell
. ~/PycharmProjects/zakupki/venv/bin/activate
```
Testing
```shell
./run_test.sh
```
OR
```shell
cd tests/ && pytest -s -v test_poisk.py
```

# Version 1.0
